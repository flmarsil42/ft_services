#!/bin/sh

mkdir -p /var/run/nginx

ssh-keygen -A
adduser --disabled-password flmarsil
echo "flmarsil:flmarsil" | chpasswd
sed 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' -i /etc/ssh/sshd_config
echo "Port 5789" >> /etc/ssh/sshd_config
/usr/sbin/sshd

nginx -g "daemon off;"