# ft_services

Le projet consiste à mettre en place une infrastructure de différents services a l'aide de Kubernetes.

Les différents services sont :
- Un serveur web (Nginx) auquel on peut accéder en http / https / ssh.
- Un serveur FTPS
- Phpmyadmin, Mysql et Wordpress fonctionnant ensemble.
- Grafana, influxDB et Telegraf fonctionnant ensemble.


Lire le [sujet][1]

`Prérequis pour lancer le projet : Docker et Minikube`

### Construire et lancer le projet

1. Installer [Docker][2] et [Minikube][3]

2. Télécharger / Cloner le repo

```
git clone https://gitlab.com/flmarsil42/ft_services.git
```

2. `cd` dans le dossier racine, et lancer `setup.sh`. Cela va lancer `minikube`, lier docker à minikube, construire toutes les images et créer tous les pods dans le cluster Kubernetes.

```
cd ft_services
./setup.sh
```
## Sources

- [Full tutorial for Docker and Kubernetes][4]
- [Comprendre le clustering et load balancing (FR)][5]
- [Cours sur kubernetes (FR)][6]
- [Use local images in kubernetes from Docker][7]
- [Use local images in kubernetes from Docker (2)][8]
- [Deploying PHP-FPM and NGINX in kubernetes][9]
- [Deploying PHP/Mysql in kubernetes][10]
- [Examples of containers build with Alpine][11]
- [Using php as web server][12]
- [Using MetalLB as loadbalancer][13]
- [Launching ft_services with VM (for 42 students during covid-19 pandemy)][14]

[1]: https://gitlab.com/flmarsil42/ft_services/-/blob/master/fr.subject.pdf
[2]: https://docs.docker.com/get-docker/
[3]: https://kubernetes.io/fr/docs/tasks/tools/install-minikube/
[4]: https://www.youtube.com/watch?v=jPdIRX6q4jA&list=PLy7NrYWoggjwPggqtFsI_zMAwvG0SqYCb&ab_channel=TechWorldwithNana
[5]: https://www.youtube.com/watch?v=9EoqLdmZCTU&ab_channel=Cookieconnect%C3%A9
[6]: https://devopssec.fr/article/cours-complet-apprendre-orchestrateur-kubernetes-k8s
[7]: https://stackoverflow.com/questions/40144138/pull-a-local-image-to-run-a-pod-in-kubernetes
[8]: https://stackoverflow.com/questions/53714508/is-possible-to-use-local-image-into-pods-yaml-in-kubernetes
[9]: https://www.digitalocean.com/community/tutorials/how-to-deploy-a-php-application-with-kubernetes-on-ubuntu-16-04
[10]: https://medium.com/faun/deploy-your-first-scaleable-php-mysql-web-application-in-kubernetes-33ed7ab66595
[11]: https://github.com/container-examples
[12]: https://www.php.net/manual/en/features.commandline.webserver.php
[13]: https://medium.com/@shoaib_masood/metallb-network-loadbalancer-minikube-335d846dfdbe
[14]: https://www.notion.so/Ft_services-VM-852d4f9b0d9a42c1a2de921e4a2ac417
