#!/bin/bash

MINIKUBE_IP=`minikube ip`

#Configuration addons Minikube
# minikube addonsenab disable ingress
# minikube addons le metallb

eval $(minikube docker-env)

#Création des images Docker
echo "=============================================="

#Nginx
echo "\033[31m\033[33m Création de l'image docker : NGINX \033[m"
docker build -t nginx_image srcs/nginx/ > /dev/null
#Wordpress
echo "\033[31m\033[33m Création de l'image docker : WORDPRESS \033[m"
docker build -t wordpress_image srcs/wordpress/ > /dev/null
#Mysql
echo "\033[31m\033[33m Création de l'image docker : MYSQL \033[m"
docker build -t mysql_image srcs/mysql/ > /dev/null
#Phpmyadmin
echo "\033[31m\033[33m Création de l'image docker : PHPMYADMIN \033[m"
docker build -t phpmyadmin_image srcs/phpmyadmin/ > /dev/null
#Influxdb
echo "\033[31m\033[33m Création de l'image docker : INFLUXDB \033[m"
docker build -t influxdb_image srcs/influxdb/ > /dev/null
#Telegraf
echo "\033[31m\033[33m Création de l'image docker : TELEGRAF \033[m"
docker build -t telegraf_image srcs/telegraf/ > /dev/null
#Grafana
echo "\033[31m\033[33m Création de l'image docker : GRAFANA \033[m"
docker build -t grafana_image srcs/grafana/ > /dev/null
#FTPS
echo "\033[31m\033[33m Création de l'image docker : FTPS \033[m"
docker build -t ftps_image srcs/ftps/ > /dev/null

echo "=============================================="

#Dèploiement des Services 
#MetalLB
echo "\033[31m\033[32m Déploiement : MetalLB \033[m"
kubectl apply -f srcs/metallb_config.yaml > /dev/null

#Mysql
echo "\033[31m\033[32m Déploiement : MYSQL\033[m"
kubectl apply -f srcs/mysql/mysql_secret.yaml > /dev/null
kubectl apply -f srcs/mysql/mysql.yaml > /dev/null

#Nginx
echo "\033[31m\033[32m Déploiement : NGINX \033[m"
kubectl apply -f srcs/nginx/nginx_secret.yaml > /dev/null
kubectl create configmap nginx-configmap --from-file=srcs/nginx/default.conf --from-file=srcs/nginx/proxy.conf > /dev/null

kubectl create configmap nginx-home --from-file=srcs/nginx/html/index.html --from-file=srcs/nginx/html/style.css > /dev/null
kubectl apply -f srcs/nginx/nginx.yaml > /dev/null

#wordpress
echo "\033[31m\033[32m Déploiement : WORDPRESS \033[m"
kubectl create configmap nginx-wp-configmap --from-file=srcs/wordpress/default.conf > /dev/null
kubectl apply -f srcs/wordpress/wordpress_secret.yaml > /dev/null
kubectl apply -f srcs/wordpress/wordpress.yaml > /dev/null

#phpmyadmin
echo "\033[31m\033[32m Déploiement : PHPMYADMIN \033[m"
kubectl create configmap nginx-phpmyadmin-configmap --from-file=srcs/phpmyadmin/default.conf > /dev/null
kubectl apply -f srcs/phpmyadmin/phpmyadmin.yaml > /dev/null

#influxdb
echo "\033[31m\033[32m Déploiement : INFLUXDB \033[m"
kubectl apply -f srcs/influxdb/influxdb-secret.yaml > /dev/null
kubectl apply -f srcs/influxdb/influxdb-conf.yaml > /dev/null
kubectl apply -f srcs/influxdb/influxdb.yaml > /dev/null

#telegraf
echo "\033[31m\033[32m Déploiement : TELEGRAPH \033[m"
kubectl apply -f srcs/telegraf/telegraf-secret.yaml > /dev/null
kubectl apply -f srcs/telegraf/telegraf-conf.yaml > /dev/null
kubectl apply -f srcs/telegraf/telegraf.yaml > /dev/null

#Grafana
echo "\033[31m\033[32m Déploiement : GRAFANA\033[m"
kubectl apply -f srcs/grafana/grafana.yaml > /dev/null

#FTPS
echo "\033[31m\033[32m Déploiement : FTPS \033[m"
kubectl apply -f srcs/ftps/ftps.yaml > /dev/null

echo "=============================================="


# echo "Execution de minikube dashboard"
# minikube dashboard

kubectl get all